/** main.c -- main program
	* version 1.0, September 17th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#include <gtk/gtk.h>
#include <string.h>
#include <stdio.h>
#include "gui.h"
#include "data.h"


static void print_help(void);


int main(int argc, char **argv)
{
	gtk_init(&argc, &argv);


	/* init the program with the position of the window
	 * and the color to use for circles */
	switch (argc) {
		case 1: set_window("default", "default"); break;

		case 2: if (!strcmp(argv[1], "-h"))
					print_help();
				else {
					printf("error: Unknown argument\n\n");
					print_help();
				}
				break;

		case 3: if (!strcmp(argv[1], "-c"))
					set_window("default", argv[2]);
				else if (!strcmp(argv[1], "-p"))
					set_window(argv[2], "default");
				else {
					printf("error: Unknown argument\n\n");
					print_help();
				}
				break;

		case 4: if (!strcmp(argv[1], "-cp"))
					set_window(argv[3], argv[2]);
				else if (!strcmp(argv[1], "-pc"))
					set_window(argv[2], argv[3]);
				else {
					printf("error: Unknown argument\n\n");
					print_help();
				}
				break;

		case 5: if (!strcmp(argv[1], "-c")  && !strcmp(argv[3], "-p"))
					set_window(argv[4], argv[2]);
				else if (!strcmp(argv[1], "-p") && !strcmp(argv[3], "-c"))
						set_window(argv[2], argv[4]);
				else {
					printf("error: Unknown argument\n\n");
					print_help();
				}
				break;

		default: printf("error: Wrong number of arguments\n");
				 print_help();
	}


	return 0;
}

static void print_help(void)
{
	printf("usage: binary-clock [options]\n\n");
	printf("options:");
	printf("-c color: specify what color the dots shuld be\n");
	printf("\tcolor can be a standard name taken from the X11 rgb.txt file\n");
	printf("\tor any valid rgb value\n");
	printf("\t-p position: specify where on screen to put the widget\n");
	printf("\tNote: position must be given as \"xpositionxyposition\"\n");
	printf("\t\te.g.: 235x456\n\n");
	printf("sample usage: binary-clock -cp yellow 500x500\n");
	printf("\t\tbinary-clock -c 255.255.255\n\n");
	printf("Note: if an option is missing then the default will\n");
	printf("\tbe used for that option\n");

	return;
}
