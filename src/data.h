/** data.h -- functions definitions
	* version 1.0, September 17th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED


/** gets the time in as a string in binary form
  * e.g.: hour == 3 --> h == 000010
  */
typedef struct {
	char h[7], m[7], s[7];
	/* used to optimize a little, traded size for cpu time */
	int min, sec;
} time_str_t;


/** returns the current time
  */
time_str_t get_time(void);


#endif
