/** gui.h -- functions definitions
	* version 1.0, September 17th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED


/** window size */
#define WINDOW_WIDTH 100
#define WINDOW_HEIGHT 55
/** window's background color (dark-grey) */
#define WINDOW_BG_COLOR 0.2, 0.2, 0.2
/** window transparency level */
#define WINDOW_ALPHA 0.8


/** set's up the main window, with all the callbacks
  * and fills the circles with the color of "color"
  */
void set_window(char *position, char *color);


#endif
