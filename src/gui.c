/** gui.c -- functions implementations for gui.h
	* version 1.0, September 17th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#include <gtk/gtk.h>
#include <cairo.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include "gui.h"
#include "data.h"


/** tells the function 'create_circle'
  * what kind of circle to draw
  */
#define CIRCLE_FULL 1
#define CIRCLE_EMPTY 2


/** window position
  */
typedef struct {
	int x, y;
} pos_t;


/** contains the images that tells wheather it
  * is on or off
  */
typedef struct {
		GtkWidget *h[6], *m[6], *s[6];
	} circle_t;


/** contains the data needed to be passed to the update
  * functions
  */
typedef struct {
	circle_t circle;
	GdkRGBA color;
	/* used to update the hour and minutes on the
	 * first run of the program */
	int run;
} update_data_t;


/** gets the window position form the string
  * in integer values, so gtk can use them
  */
static pos_t transform_window_pos(char *pos)
{
	pos_t p;
	char *pch = NULL, ds[5];


	/* get x position
	 * this is accomplished by finding the position of the 'x'
	 * in string and then copy the characters up to that position
	 * in 'ds', finally converting 'ds' to int */
	pch = strchr(pos, 'x');
	strncpy(ds, pos, pch-pos);
	p.x = atoi(ds);

	/* get y position
	 * done by filling the 'pos' string wih '0' (zeros) up to (including)
	 * 'x', and then converting the string to int.
	 * works because atoi discards leading zeros in the string when converting */
	memset(pos, '0', pch-pos+1);
	p.y = atoi(pos);

	return p;
}


/** for convenience the user enter the color in rgb
  * as "rrr.bbb.ggg", but gtk requires the color to
  * be as "rgb(rrr,bbb,ggg)". This function does
  * just this
  */
static char* transform_color(char *color)
{
	char *c = NULL;
	char *pch = NULL;

	/* replace '.' with ',' */
	pch = strchr(color, '.');
	color[pch-color] = ',';

	pch = strchr(pch+1, '.');
	color[pch-color] = ',';

	/* cat the string in it's final form */
	c = malloc((strlen(color)+5) * sizeof(*c));
	strcpy(c, "rgb(");
	strcat(c, color);
	strcat(c, ")");

	return c;
}


static void on_screen_change(GtkWidget *widget, int *hasAlpha)
{
	GdkScreen *screen = gtk_widget_get_screen(widget);
	GdkVisual *visual = gdk_screen_get_rgba_visual(screen);


	if (!visual)
		printf("Your screen doesn't supports transparency\n");
	else
		*hasAlpha = 1;

	gtk_widget_set_visual(widget, visual);

	g_object_unref(visual);

	return;
}


static int on_draw_event(GtkWidget *widget, int *hasAlpha)
{
	cairo_t *cr = gdk_cairo_create(gtk_widget_get_window(widget));


	if (hasAlpha)
		cairo_set_source_rgba(cr, WINDOW_BG_COLOR, WINDOW_ALPHA);
	else
		cairo_set_source_rgb(cr, WINDOW_BG_COLOR);

	/* draw bg */
	cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cr);


	/* draw border */
	if (hasAlpha)
		cairo_set_source_rgba(cr, 1, 1, 1, WINDOW_ALPHA);
	else
		cairo_set_source_rgb(cr, 1, 1, 1);

	cairo_set_line_width(cr, 4);
	cairo_rectangle(cr, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	cairo_stroke(cr);
	cairo_fill(cr);

	cairo_destroy(cr);

	return 0;
}


/** creates the pixbuf for a circle
  */
static GdkPixbuf* create_circle(int type, GdkRGBA *col)
{
	GdkPixbuf *pix;
	cairo_surface_t *surface;
	cairo_t *cr;
	/* width x height of the circle */
	int width, height;


	/* set up the image in memory */
	width = height = 15;
	surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);

	/* draw the circle */
	cr = cairo_create(surface);
	cairo_set_line_width(cr, 0.5);
	if (type == CIRCLE_FULL) {
		cairo_save(cr);
		cairo_arc(cr, width/2, height/2, 6.5, 0, 2*M_PI);
		/* set the color of the circle to the color given by the user */
		gdk_cairo_set_source_rgba(cr, col);
		cairo_fill_preserve(cr);
		cairo_restore(cr);
		cairo_stroke(cr);
	} else {
		cairo_set_source_rgba(cr, 0, 0, 0, 1);
		cairo_arc(cr, width/2, height/2, 6.5, 0, 2*M_PI);
		cairo_stroke(cr);
	}

	pix = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);

	cairo_surface_destroy(surface);
	cairo_destroy(cr);

	return pix;
}


/** continously check for the time and updates the circles
  * on the screen to reflect the current time
  */
static int update(update_data_t *data)
{
	time_str_t binTime;
	GdkPixbuf *circleFull, *circleEmpty;
	int i;


	binTime = get_time();
	circleFull = create_circle(CIRCLE_FULL, &data->color);
	circleEmpty = create_circle(CIRCLE_EMPTY, NULL);


	switch (data->run) {
		case 1: for (i = 0; i < 6; i++) {
					/* update hour */
					/* note to self: research: wouldn't be better/safer
					 * to use strcmp? */
					if (binTime.h[i] == '1')
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.h[i]), circleFull);
					else
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.h[i]), circleEmpty);

					/* update min */
					if (binTime.m[i] == '1')
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.m[i]), circleFull);
					else
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.m[i]), circleEmpty);

					/* update sec */
					if (binTime.s[i] == '1')
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.s[i]), circleFull);
					else
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.s[i]), circleEmpty);
				}
				data->run = 2;
				break;

		/* note to self: not tu much of an optimization, is it? */
		case 2: for (i = 0; i < 6; i++) {
					/* update hour, if necessary */
					if (binTime.min == 0) {
						if (binTime.h[i] == '1')
							gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.h[i]), circleFull);
						else
							gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.h[i]), circleEmpty);
					}

					/* update minutes, necessary */
					if (binTime.sec == 0) {
						if (binTime.m[i] == '1')
							gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.m[i]), circleFull);
						else
							gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.m[i]), circleEmpty);
					}

					/* update seconds */
					if (binTime.s[i] == '1')
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.s[i]), circleFull);
					else
						gtk_image_set_from_pixbuf(GTK_IMAGE(data->circle.s[i]), circleEmpty);
				}
	}

	g_object_unref(circleFull);
	g_object_unref(circleEmpty);

	return 1;	/* so the callback continues to be called */
}


void set_window(char *position, char *color)
{
	pos_t pos;
	update_data_t data;
	GtkWidget *window, *vbox, *hbox;
	int hasAlpha = 0, i;


	/* get position and color (if supplied otherwise fills in some
	 * some defaults) in datatypes gtk can use */
	if (!strcmp(position, "default")) {
		pos.x = 0;
		pos.y = 0;
	} else
		pos = transform_window_pos(position);

	if (!strcmp(color, "default"))
		gdk_rgba_parse(&data.color, "green");
	else if (isdigit(color[0])) {
		color = transform_color(color);
		if (!gdk_rgba_parse(&data.color, color)) {
			printf("error: Unknown color %s\n", color);
			return;
		}
		free(color);
	} else
		if (!gdk_rgba_parse(&data.color, color)) {
			printf("error: Unknown color %s\n", color);
			return;
		}

	/* set up window */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "binary-clock");
	gtk_window_set_resizable(GTK_WINDOW(window), 0);
	gtk_window_set_decorated(GTK_WINDOW(window), 0);
	gtk_window_stick(GTK_WINDOW(window));
	gtk_window_set_keep_below(GTK_WINDOW(window), 1);
	gtk_window_set_skip_taskbar_hint(GTK_WINDOW(window), 1);
	gtk_window_set_skip_pager_hint(GTK_WINDOW(window), 1);
	gtk_window_set_default_size(GTK_WINDOW(window), WINDOW_WIDTH, WINDOW_HEIGHT);
	gtk_window_set_accept_focus(GTK_WINDOW(window), 0); /* so the widget doesn't distrub the user */
	gtk_container_set_border_width(GTK_CONTAINER(window), 5);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	/* put the window at the required position
	 * note: docs say this function can fail */
	gtk_window_move(GTK_WINDOW(window), pos.x, pos.y);
	/* make the window transparent */
	gtk_widget_set_app_paintable(window, 1);
	g_signal_connect(window, "draw", G_CALLBACK(on_draw_event), &hasAlpha);
	g_signal_connect(window, "screen-changed", G_CALLBACK(on_screen_change), &hasAlpha);
	on_screen_change(window, &hasAlpha);

	/* create and pack the circles
	 * 1st row: hour
	 * 2nd row: minutes
	 * 3rd row: seconds */
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(window), vbox);

	/* hour */
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, 0, 0, 0);

	for (i = 0; i < 6; i++) {
		data.circle.h[i] = gtk_image_new();
		gtk_box_pack_start(GTK_BOX(hbox), data.circle.h[i], 0, 0, 0);
	}

	/* minutes */
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, 0, 0, 0);

	for (i = 0; i < 6; i++) {
		data.circle.m[i] = gtk_image_new();
		gtk_box_pack_start(GTK_BOX(hbox), data.circle.m[i], 0, 0, 0);
	}

	/* seconds */
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, 0, 0, 0);

	for (i = 0; i < 6; i++) {
		data.circle.s[i] = gtk_image_new();
		gtk_box_pack_start(GTK_BOX(hbox), data.circle.s[i], 0, 0, 0);
	}


	/* mark it as the first run, so the hour and minute
	 * gets displayed, failure to do so will cause
	 * only the seconds to show up */
	data.run = 1;

	/* to make the clock work */
	g_timeout_add(500, (GSourceFunc) update, &data);


	gtk_widget_show_all(window);
	gtk_main();

	return;
}
