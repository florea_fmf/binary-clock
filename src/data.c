/** data.c -- functions implementations for data.h
	* version 1.0, September 17th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#include <glib.h>
#include "data.h"


/** contains the interesting stuff about time
  */
typedef struct {
	int hour, min, sec;
} time_int_t;


/** gets the current time as integer values
  */
static time_int_t get_time_int(void)
{
	GDateTime *time = NULL;
	time_int_t t;


	time = g_date_time_new_now_local();

	t.hour = g_date_time_get_hour(time);
	t.min = g_date_time_get_minute(time);
	t.sec = g_date_time_get_second(time);

	g_date_time_unref(time);

	return t;
}


/** transform the integer values of the time in
  * their binary counterparts
  */
static time_str_t transform_time(time_int_t t)
{
	time_str_t t2;
	int i;
	char dh[7], dm[7], ds[7];


	for (i = 0; i < 6; i++)
		dh[i] = dm[i] = ds[i] = '0';
	dh[6] = dm[6] = ds[6] = '\0';
	t2.h[6] = t2.m[6] = t2.s[6] = '\0';

	/* format hour */
	i = 0;
	while (t.hour > 0) {
		if (t.hour % 2 != 0)
			dh[i] = '1';
		i++;
		t.hour /= 2;
	}

	/* format minutes */
	i = 0;
	while (t.min > 0) {
		if (t.min % 2 != 0)
			dm[i] = '1';
		i++;
		t.min /= 2;
	}

	// format secods
	i = 0;
	while (t.sec > 0) {
		if (t.sec % 2 != 0)
			ds[i] = '1';
		i++;
		t.sec /= 2;
	}

	/* NOTE: the actual binary number is get by
	 * writting the string backwards */
	for (i = 0; i < 6; i++) {
		t2.h[i] = dh[5-i];
		t2.m[i] = dm[5-i];
		t2.s[i] = ds[5-i];
	}

	return t2;
}


time_str_t get_time(void)
{
	time_str_t t;
	time_int_t t2;


	t2 = get_time_int();

	t = transform_time(t2);
	t.min = t2.min;
	t.sec = t2.sec;

	return t;
}
